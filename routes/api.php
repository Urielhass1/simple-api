<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/cep', 'API\CepController@index')->name('cep.index');

Route::post('/cep', 'API\CepController@store')->name('cep.store');

Route::get('/cep/{cep}', 'API\CepController@show')->name('cep.show');

Route::put('/cep/{cep}', 'API\CepController@update')->name('cep.update');

Route::delete('/cep/{cep}', 'API\CepController@destroy')->name('cep.destroy');
