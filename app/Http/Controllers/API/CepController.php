<?php

namespace App\Http\Controllers\API;

use App\Cep;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ModelFilters\CepFilter;
use Validator;

class CepController extends Controller
{
    public function index(Request $request)
    {
        $ceps = Cep::filter($request->all(), CepFilter::class)->get();

        return response()->json([
            'message' => 'Lista dos Endereços',
            'data' => $ceps
        ]);
    }

    public function update(Request $request, Cep $cep)
    {
        $validator = Validator::make($request->all(), [
            'cep' => 'nullable',
            'logradouro' => 'nullable',
            'complemento' => 'nullable',
            'bairro' => 'nullable',
            'cidade' => 'nullable',
            'estado' => 'nullable',
            'estado_sigla' => 'nullable',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()], 400);
        }

        $cep->update($request->all());

        return response()->json([
            'status' => 'success',
            'message' => 'Cep atualizado',
            'data' => $cep
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cep' => 'required',
            'logradouro' => 'required',
            'complemento' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            'estado_sigla' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()], 400);
        }

        $id = count(Cep::all()->toArray()) + 1;
        $all = $request->all();
        $result = array_merge($all, ['id' => $id]);

        $cep = Cep::create($result);

        return response()->json([
            'status' => 'success',
            'message' => 'Novo endereço adicionado',
            'data' => $cep
        ]);
    }

    public function show(Cep $cep)
    {
        return response()->json([
            'status' => 'success',
            'message' => 'CEP Encontrado',
            'data' => $cep
        ]);
    }

    public function destroy(Cep $cep)
    {
        $cep->delete();

        return response()->json([
            'message' => 'CEP removido!',
            'status' => 'success',
            'data' => []
        ]);
    }
}
