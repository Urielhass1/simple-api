<?php namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class CepFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function cep($str)
    {
        return $this->where('cep', 'like', "%{$str}%");
    }

    public function cidade($str)
    {
        return $this->where('cidade', 'like', "%{$str}%");
    }

    public function logradouro($str)
    {
        return $this->where('logradouro', 'like', "%{$str}%");
    }

    public function complemento($str)
    {
        return $this->where('complemento', 'like', "%{$str}%");
    }

    public function bairro($str)
    {
        return $this->where('bairro', 'like', "%{$str}%");
    }

    public function estado($str)
    {
        return $this->where('estado', 'like', "%{$str}%");
    }

    public function estado_sigla($str)
    {
        return $this->where('estado_sigla', 'like', "%{$str}%");
    }

}
