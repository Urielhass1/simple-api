<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use EloquentFilter\Filterable;

class Cep extends Model
{
    use Filterable;

    protected $fillable = [
        'cep',
        'logradouro',
        'complemento',
        'bairro',
        'id',
        'cidade',
        'estado',
        'estado_sigla'
    ];
}
