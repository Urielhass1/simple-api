<?php

use Illuminate\Database\Seeder;

class CepCidadeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = new \DateTime();

        DB::table('ceps')->insert([
            "cep" => "89227-100",
            "logradouro" => "Rua Tangará",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-110",
            "logradouro" => "Rua Marquês de Pombal",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-120",
            "logradouro" => "Rua Francisca Klein Salfer",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-130",
            "logradouro" => "Rua Arco-Íris",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-140",
            "logradouro" => "Rua Finlândia",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-150",
            "logradouro" => "Rua Palma Sola",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-160",
            "logradouro" => "Rua Salto Veloso",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-170",
            "logradouro" => "Rua Professor João Martins Veras",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-180",
            "logradouro" => "Rua Cerro Verde",
            "complemento" => "até 448/449",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-190",
            "logradouro" => "Rua Presidente Kennedy",
            "complemento" => "",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-200",
            "logradouro" => "Rua Toríbio Soares Pereira",
            "complemento" => "até 998/999",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
        DB::table('ceps')->insert([
            "cep" => "89227-210",
            "logradouro" => "Rua Fraiburgo",
            "complemento" => "até 398/399",
            "bairro" => "Iririú",
            "cidade" => "Joinville",
            "estado" => "Santa Catarina",
            "estado_sigla" => "sc",
            "created_at" => $date,
            "updated_at" => $date
        ]);
    }
}
