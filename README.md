# simple-api
## Configuração Base
* Projeto em Laravel
* PHP 7.3
* Apache2 
* Composer 1.8.3
* SQLite

## Para executar
* Clonar o repositório
* Acessar a pasta clonada
* `composer install`
* `php artisan key:generate`
	* Verificar se a pasta de LOG tem permissão para escrita
* Alterar o arquivo `.env` para as configurações da máquina local
	* DB_DATABASE colocar o caminho inteiro até o arquivo, exemplo
	* DB_DATABASE=/var/www/html/simple-api/storage/database.sqlite (caso o arquivo não exista, é necessário criar)
    * Caso o banco esteja dando problema, verificar permissão do arquivo e dar permissão de escrita e leitura
        * `chmod -R 775 database` dentro da raiz do projeto (PARA LINUX)
* `php artisan migrate` Gerar o banco de dados SQLite
* `php artisan db:seed` Gerar alguns registros pré cadastrados para teste
* `php artisan serve` Rodar o servidor e acessar a URL /api/cep

## CRUD completo para api de CEP
* GET /api/cep irá realizar a consulta
	* /api/cep?cep=89227-100 irá trazer baseado no parametro que foi passado
		* Disponíveis para consulta (cep, cidade, logradouro, complemento, bairro, estado, estado_sigla)
* GET /api/cep/{id_cep} irá consulta o CEP com o id especifico
* DELETE /api/cep/{id_cep} irá remover o CEP
* POST /api/cep irá cadastrar um novo, se estiver com o formato correto e todos os campos que são obrigatórios preenchidos
* PUT /api/cep/{id_cep} irá atualizar os campos, será necessário passar o id do CEP para ele atualizar.

### Observações
* Não é necessário realizar login, projeto foi criado apenas para aplicação de API publica
* No momento ainda não foi criado nenhum relacionamento de pais/estado/cidade/cep
